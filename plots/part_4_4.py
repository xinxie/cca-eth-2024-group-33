import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from matplotlib.gridspec import GridSpec
from datetime import datetime, timedelta

# Define color mapping
job_colors = {
    'blackscholes': '#CCA000',
    'canneal': '#CCCCAA',
    'dedup': '#CCACCA',
    'ferret': '#AACCCA',
    'freqmine': '#0CCA00',
    'radix': '#00CCA0',
    'vips': '#CC0A00',
    'scheduler': '#808080',  # Grey for scheduler
    'memcached': '#000000'   # Black for memcached
}

job_list = [
    "blackscholes",
    "canneal",
    "dedup",
    "ferret",
    "freqmine",
    "radix",
    "vips",
    "scheduler",
    "memcached"
]

# Exclude memcached and scheduler for B plots
job_list_B = [
    "blackscholes",
    "canneal",
    "dedup",
    "ferret",
    "freqmine",
    "radix",
    "vips"
]

# Define job to id mapping and colors
job2id = {job: i for i, job in enumerate(job_list_B)}
colors = [job_colors[job] for job in job_list_B]
id2job = job_list_B

def to_step(y, x):
    step_x = np.repeat(x, 2)[1:-1]
    step_y = np.repeat(y, 2)
    return step_x, step_y

# Function to read and parse job log files
def parse_log(file_path):
    events = []
    with open(file_path, 'r') as file:
        for line in file:
            timestamp, event, *details = line.strip().split(' ')
            timestamp = datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%f")
            details = ' '.join(details)
            events.append((timestamp, event, details))
    return events

# Function to read and parse mcperf files
def parse_mcperf(file_path):
    p95_values = []
    qps_values = []
    qps_target_values = []
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith("read"):
                parts = line.strip().split()
                p95 = float(parts[12])
                qps = float(parts[-2])
                qps_target = float(parts[-1])
                p95_values.append(p95)
                qps_values.append(qps)
                qps_target_values.append(qps_target)
    return p95_values, qps_values, qps_target_values

# Generate relative timestamps
def create_relative_timestamps(events):
    start_time = events[0][0]
    relative_events = [(event[0] - start_time, event[1], event[2]) for event in events]
    return relative_events

# Process events and generate relative timestamps
relative_events_list = []
log_files = ['jobs_1.txt', 'jobs_2.txt', 'jobs_3.txt']
for log_file in log_files:
    events = parse_log(log_file)
    relative_events = create_relative_timestamps(events)
    relative_events_list.append(relative_events)

# Read mcperf files and generate timestamps
mcperf_files = ['mcperf_1.txt', 'mcperf_2.txt', 'mcperf_3.txt']
all_p95_values = []
all_qps_values = []
all_qps_target_values = []

for mcperf_file in mcperf_files:
    p95_values, qps_values, qps_target_values = parse_mcperf(mcperf_file)
    all_p95_values.append(p95_values)
    all_qps_values.append(qps_values)
    all_qps_target_values.append(qps_target_values)

# Extract relevant information and organize data
def process_events(relative_events):
    job_events = {}
    core_updates = {}
    for job in job_list:
        job_events[job] = {'start': [], 'end': [], 'pause': [], 'unpause': []}
        core_updates[job] = []

    for event in relative_events:
        timestamp, event_type, details = event
        timestamp = timestamp.total_seconds()  # Convert timedelta to seconds
        if event_type == 'start' and len(details.split(' ')) > 1:
            job_name = details.split(' ')[0]
            cores = details.split(' ')[1]
            job_events[job_name]['start'].append((timestamp, cores))
        elif event_type == 'end':
            job_name = details.split(' ')[0]
            job_events[job_name]['end'].append(timestamp)
        elif event_type == 'pause':
            job_name = details.split(' ')[0]
            job_events[job_name]['pause'].append(timestamp)
        elif event_type == 'unpause':
            job_name = details.split(' ')[0]
            job_events[job_name]['unpause'].append(timestamp)
        elif event_type == 'update_cores':
            job_name = details.split(' ')[0]
            cores = details.split(' ')[1]
            num_cores = len(cores.strip('[]').split(','))
            core_updates[job_name].append((timestamp, num_cores))

    return job_events, core_updates

# Function to plot combined p95 latency (A plot) and job events timeline (B plot)
def plot_combined(job_events, core_updates, p95_values, qps_values, qps_target_values, run_number):
    # Create figure and axes for the 3 subplots (vertical layout)
    fig = plt.figure(figsize=(16, 8))
    gs = GridSpec(5, 1)
    gs.update(hspace=0.5)
    ax1 = fig.add_subplot(gs[0:2, 0])
    ax2 = fig.add_subplot(gs[2:4, 0])
    ax3 = fig.add_subplot(gs[4:5, 0])

    # Generate fixed timestamps for the A plot
    p95_timestamps = np.arange(0, len(p95_values) * 5, 5)

    # Plot p95 latency as a line plot
    ax1.plot(p95_timestamps, np.array(p95_values)/1000, color='red', marker='s', label='p95', alpha=0.7)
    ax1.set_ylabel('95th Percentile Latency (ms)')
    ax1.axhline(y=1, color='black', linestyle='--', label='SLO=1ms')  # Add horizontal line for SLO=1ms
    # ax1.text(p95_timestamps[10], 1, 'SLO=1ms', color='black', verticalalignment='bottom')  # Add label for SLO line

    ax1.set_ylim(0, 1.75)
    ax1.set_xlim(0, 700)
    ax1.set_title(f'{run_number}A')
    ax1.xaxis.set_major_locator(MultipleLocator(100))
    ax1.grid(True)

    # Plot QPS on the right y-axis
    ax1_qps = ax1.twinx()
    ax1_qps.plot(p95_timestamps, np.array(qps_values) / 1000, color='darkblue', marker='^', label='QPS', alpha=0.7)
    ax1_qps.set_ylabel('QPS (K)')
    # ax1_qps.grid(True)

    ax1_qps.set_ylim(0, 140)
    
    
    
    ax1.legend(loc='upper left')
    ax1_qps.legend(loc='upper right')

    # Set x-axis limit to 700 seconds for plot B
    ax2.set_xlim([0, 700])
    ax2.set_title(f'{run_number}B')
    # ax2.grid(True)

    # Plot CPU usage for memcached
    memcached_cores = core_updates['memcached']
    memcached_x = [0] + [core[0] for core in memcached_cores]
    memcached_y = [2] + [core[1] for core in memcached_cores]  # Initial value is 2 CPUs
    
    last_timestamp = max([max(events['end']) if events['end'] else 0 for events in job_events.values()])
    memcached_x.append(last_timestamp)
    memcached_y.append(memcached_y[-1])
    
    memcached_x_step, memcached_y_step = to_step(memcached_y, memcached_x)
    ax2.plot(memcached_x_step, memcached_y_step[:-2], color='red', linestyle='--', label='#CPUs for memcached')
    ax2.set_ylim(0, 3)
    ax2.set_ylabel('# CPU Cores')

    # Plot QPS on the right y-axis
    ax2_qps = ax2.twinx()
    ax2_qps.plot(p95_timestamps, np.array(qps_values) / 1000, color='darkblue', marker='^', label='QPS', alpha=0.7)
    ax2_qps.set_ylabel('QPS (K)')
    ax2_qps.set_ylim(0, 140)

    # Add the legend in the top right of plot B
    ax2.legend(loc='upper left')
    ax2_qps.legend(loc='upper right')

    # Adjust x-axis annotation for B to be every 100 seconds
    ax2_qps.xaxis.set_major_locator(MultipleLocator(100))

    # Plot C: job timeline
    job_positions = {job: i for i, job in enumerate(job_list_B)}
    for job, events in job_events.items():
        if job not in job_list_B:
            continue  # Skip memcached and scheduler

        start_times = [event[0] for event in events['start']]
        end_times = events['end']
        pause_times = events['pause']
        unpause_times = events['unpause']
        color = job_colors.get(job, '#000000')  # Default to black if job is not in the color map
        ypos = job_positions[job] + 0.5  # Adjust for better visual separation
        
        # Plot start and end times
        for start, end in zip(start_times, end_times):
            ax3.plot([start, end], [ypos, ypos], color=color, lw=8, solid_capstyle='butt')
        
        # Plot pause and unpause times
        for pause, unpause in zip(pause_times, unpause_times):
            ax3.plot([pause, unpause], [ypos, ypos], color='grey', lw=8, solid_capstyle='butt')

    ax3.set_xlim(0, 700)
    ax3.set_yticks([job_positions[job] + 0.5 for job in job_list_B])
    ax3.set_yticklabels([f"{job}(c)" for job in job_list_B])
    ax3.set_ylim(-0.5, len(job_list_B) - 0)
    ax3.set_ylabel('Job names')
    ax3.set_xlabel('Time (s)')

    # Add legend to explain colors in plot C
    grey_patch = plt.Line2D([0], [0], color='grey', lw=8, solid_capstyle='butt', label='Paused')
    ax3.legend(handles=[grey_patch], loc='upper right')
    
    # Annotate the time on x-axis at the end of the last job
    last_timestamp_annot = last_timestamp
    ax3.annotate(f"{last_timestamp_annot:.0f}", xy=(last_timestamp_annot, -0.5), xytext=(last_timestamp_annot, -1.75),
                #  arrowprops=dict(facecolor='black', shrink=0.05),
                 horizontalalignment='center', verticalalignment='center')
    
    # Draw a line from the end of the last job to the x-axis
    ax1.axvline(x=last_timestamp_annot, color='black', linestyle='--')
    ax2.axvline(x=last_timestamp_annot, color='black', linestyle='--')
    ax3.axvline(x=last_timestamp_annot, color='black', linestyle='--')

    # plt.tight_layout()
    plt.savefig(f"part4_4_{run_number}.png", bbox_inches='tight')
    # plt.show()

# Process and plot data for each run
for i, relative_events in enumerate(relative_events_list):
    job_events, core_updates = process_events(relative_events)
    plot_combined(job_events, core_updates, all_p95_values[i], all_qps_values[i], all_qps_target_values[i], i + 1)
