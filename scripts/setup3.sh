#!/bin/bash

# set up cluster
# export KOPS_STATE_STORE=<your-gcp-state-store>
PROJECT=`gcloud config get-value project`
kops create -f ../cloud-comp-arch-project/part3.yaml
kops create secret --name part3.k8s.local sshpublickey admin -i ~/.ssh/cloud-computing.pub
kops update cluster part3.k8s.local --yes --admin
kops validate cluster --wait 10m
kubectl get nodes -o wide

# create memcached server (can modify the running node) 
kubectl create -f ../part3/memcache-t1-cpuset.yaml
kubectl expose pod some-memcached --name some-memcached-11211 --type LoadBalancer --port 11211 --protocol TCP
sleep 60
kubectl get service some-memcached-11211
kubectl get pods -o wide

# install and run mcperf
nodes="client-agent-a client-agent-b client-measure"
for node in $nodes; do
    echo "installing mcperf in ${node}"
    name=$(kubectl get nodes -l cca-project-nodetype=$node -o jsonpath='{.items[*].metadata.name}')
    gcloud compute scp --ssh-key-file ~/.ssh/cloud-computing ./install_mcperf.sh ubuntu@$name:~/ --zone europe-west3-a
    gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "bash ~/install_mcperf.sh"
    if [ $node == "client-agent-a" ]; then
        echo "${name}: mcperf -T 2 -A"
        gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "nohup ~/memcache-perf-dynamic/mcperf -T 2 -A > /dev/null 2>&1 &"
    elif [ $node == "client-agent-b" ]; then
        echo "${name}: mcperf -T 4 -A"
        gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "nohup ~/memcache-perf-dynamic/mcperf -T 4 -A > /dev/null 2>&1 &"
    elif [ $node == "client-measure" ]; then
        echo "setting up client-measure"
        memcached_ip=$(kubectl get pod some-memcached -o jsonpath='{.status.podIP}')
        agent_a_ip=$(kubectl get nodes -l cca-project-nodetype="client-agent-a" -o jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}")
        agent_b_ip=$(kubectl get nodes -l cca-project-nodetype="client-agent-b" -o jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}")
        gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "~/memcache-perf-dynamic/mcperf -s ${memcached_ip} --loadonly"
        gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "nohup ~/memcache-perf-dynamic/mcperf -s ${memcached_ip} -a ${agent_a_ip} -a ${agent_b_ip} --noload -T 6 -C 4 -D 4 -Q 1000 -c 4 -t 10 --scan 30000:30500:5 > ~/mcperf.txt 2>&1 &"
    fi
done

echo "done"

