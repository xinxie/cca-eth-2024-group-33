import psutil
import time

if __name__ == '__main__':
    f = open("/home/ubuntu/cpu_usage.csv", 'w')
    f.write("timestamp,cpu0,cpu01\n")

    for i in range(150):
        ts = int(time.time())
        cpu_percent = psutil.cpu_percent(interval=2, percpu=True)
        cpu0 = cpu_percent[0]
        cpu01 = cpu_percent[0] + cpu_percent[1]
        f.write("%s,%s,%s\n" % (ts,cpu0,cpu01))
        f.flush()
        # time.sleep(1)

