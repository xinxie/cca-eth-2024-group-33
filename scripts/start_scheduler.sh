#!/bin/bash

# use for part4
scheduler_path="../src/scheduler4.py"
mcperf_param="--noload -T 16 -C 4 -D 4 -Q 1000 -c 4 -t 820 --qps_interval 10 --qps_min 5000 --qps_max 100000 --qps_seed 3274"

memcached_name=$(kubectl get nodes -l cca-project-nodetype="memcached" -o jsonpath='{.items[*].metadata.name}')
measure_name=$(kubectl get nodes -l cca-project-nodetype="client-measure" -o jsonpath='{.items[*].metadata.name}')
memcached_ip=$(kubectl get nodes -l cca-project-nodetype="memcached" -o jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}")
agent_ip=$(kubectl get nodes -l cca-project-nodetype="client-agent" -o jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}")

gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$memcached_name --zone europe-west3-a --command "pid=\$(pidof memcached) && sudo taskset -a -cp 0,1 \$pid"
gcloud compute scp --ssh-key-file ~/.ssh/cloud-computing $scheduler_path ubuntu@$memcached_name:~/ --zone europe-west3-a

gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$measure_name --zone europe-west3-a --command "~/memcache-perf-dynamic/mcperf -s ${memcached_ip} --loadonly"

# echo "start scheduler"
# gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$memcached_name --zone europe-west3-a --command "nohup sleep 1 && python3 $scheduler_path > /dev/null 2>&1 &"

echo "start mcperf measure"
gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$measure_name --zone europe-west3-a --command "~/memcache-perf-dynamic/mcperf -s ${memcached_ip} -a ${agent_ip} ${mcperf_param}"

# gcloud compute scp --ssh-key-file ~/.ssh/cloud-computing ubuntu@$memcached_name:~/log*.txt ../data/part_4_3/  --zone europe-west3-a