#!/bin/bash

# use for part4

name=$(kubectl get nodes -l cca-project-nodetype="client-measure" -o jsonpath='{.items[*].metadata.name}')
memcached_ip=$(kubectl get nodes -l cca-project-nodetype="memcached" -o jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}")
agent_ip=$(kubectl get nodes -l cca-project-nodetype="client-agent" -o jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}")
gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "~/memcache-perf-dynamic/mcperf -s ${memcached_ip} --loadonly"
gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "~/memcache-perf-dynamic/mcperf -s ${memcached_ip} -a ${agent_ip} --noload -T 16 -C 4 -D 4 -Q 1000 -c 4 -t 5 --scan 5000:125000:5000"