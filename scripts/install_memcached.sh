config_file="/etc/memcached.conf"

# thread_number set to 2, the memcache server has 4 cpu cores, still can modify
thread_number=2

sudo apt update
sudo apt install -y memcached libmemcached-tools python3-pip
sudo systemctl status memcached
sudo sed -i "s/-m [0-9]\+/-m 1024/" $config_file
sudo sed -i "s/-l 127.0.0.1/-l ${1}/" $config_file

sudo bash -c "echo '-t ${thread_number}' >> '$config_file'"

sudo systemctl restart memcached
sudo systemctl status memcached


# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo groupadd docker
sudo usermod -a -G docker ubuntu
# newgrp docker

sudo docker pull anakli/cca:parsec_blackscholes
sudo docker pull anakli/cca:parsec_canneal
sudo docker pull anakli/cca:parsec_dedup
sudo docker pull anakli/cca:parsec_ferret
sudo docker pull anakli/cca:parsec_freqmine
sudo docker pull anakli/cca:splash2x_radix
sudo docker pull anakli/cca:parsec_vips

pip3 install psutil docker
