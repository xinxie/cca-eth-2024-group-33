#!/bin/bash

name=$(kubectl get nodes -l cca-project-nodetype="memcached" -o jsonpath='{.items[*].metadata.name}')
gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a 