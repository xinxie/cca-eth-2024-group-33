#!/bin/bash

# set up cluster
# export KOPS_STATE_STORE=<your-gcp-state-store>
PROJECT=`gcloud config get-value project`
kops create -f ../cloud-comp-arch-project/part4.yaml
kops create secret --name part4.k8s.local sshpublickey admin -i ~/.ssh/cloud-computing.pub
kops update cluster part4.k8s.local --yes --admin
kops validate cluster --wait 10m
kubectl get nodes -o wide

# install memcached
memcached_ip=$(kubectl get nodes -l cca-project-nodetype="memcached" -o jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}")
memcached_name=$(kubectl get nodes -l cca-project-nodetype="memcached" -o jsonpath='{.items[*].metadata.name}')
gcloud compute scp --ssh-key-file ~/.ssh/cloud-computing ./install_memcached.sh ubuntu@$memcached_name:~/ --zone europe-west3-a
gcloud compute scp --ssh-key-file ~/.ssh/cloud-computing ../cloud-comp-arch-project/scheduler_logger.py ubuntu@$memcached_name:~/ --zone europe-west3-a
gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$memcached_name --zone europe-west3-a --command "bash ~/install_memcached.sh ${memcached_ip}"

# install and run mcperf
nodes="client-agent client-measure"
for node in $nodes; do
    echo "installing mcperf in ${node}"
    name=$(kubectl get nodes -l cca-project-nodetype=$node -o jsonpath='{.items[*].metadata.name}')
    gcloud compute scp --ssh-key-file ~/.ssh/cloud-computing ./install_mcperf.sh ubuntu@$name:~/ --zone europe-west3-a
    gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "bash ~/install_mcperf.sh"
    if [ $node == "client-agent" ]; then
        echo "${name}: mcperf -T 16 -A"
        gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "nohup ~/memcache-perf-dynamic/mcperf -T 16 -A > /dev/null 2>&1 &"
    fi
done

echo "done"
