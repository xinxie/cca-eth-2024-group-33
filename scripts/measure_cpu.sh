#!/bin/bash

# use for part4

cp="0"
name=$(kubectl get nodes -l cca-project-nodetype="memcached" -o jsonpath='{.items[*].metadata.name}')

gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "pid=\$(pidof memcached) && sudo taskset -a -cp $cp \$pid"

gcloud compute scp --ssh-key-file ~/.ssh/cloud-computing ./monitor_cpu.py ubuntu@$name:~/ --zone europe-west3-a
gcloud compute ssh --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name --zone europe-west3-a --command "python3 ~/monitor_cpu.py"

gcloud compute scp --ssh-key-file ~/.ssh/cloud-computing ubuntu@$name:~/cpu_usage.csv ../data/part_4_1/cpu_usage.csv  --zone europe-west3-a

