# cca-eth-2024-group-33

## Warning
Make sure to update the partX.yaml file with the name of your ConfigBase


kops delete cluster --name part3.k8s.local --yes

kops delete cluster --name part4.k8s.local --yes

## Plot for Part4.3 Usage

- Install required libraries:
```bash
pip install pandas numpy seaborn matplotlib
```

- Save Logs and p95 Files as `jobs_1.txt`, ..., `jobs_3.txt`, and `mcperf_1.txt`, ..., `mcperf_3.txt`, respectively.

- Run following command:

```bash
cd part_4_3_results_group_033
python ../plots/part_4_3.py 
```