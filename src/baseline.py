import time
import psutil
import docker
from scheduler_logger import SchedulerLogger,Job

client = docker.from_env()

# Stop and remove each container
containers = client.containers.list(all=True)
for container in containers:
    container.stop()
    container.remove()

logger = SchedulerLogger()

job_list = [
    "blackscholes",
    "canneal",
    "dedup",
    "ferret",
    "freqmine",
    "radix",
    "vips"
]

def start_job(job):
    image = f"anakli/cca:splash2x_{job}" if job == "radix" else f"anakli/cca:parsec_{job}"
    command = f"./run -a run -S splash2x -p {job} -i native -n 2" if job == "radix" else f"./run -a run -S parsec -p {job} -i native -n 2"
    client.containers.run(
        image=image,
        name=job,
        detach=True,
        cpuset_cpus="2-3",
        command=command
    )
       
    logger.job_start(Job._value2member_map_.get(job), [2, 3], 2)
    print("start:", job)

def check_end(job) -> bool:
    container = client.containers.get(job)
    if(container.attrs['State']['Status'] == "exited"):
        logger.job_end(Job._value2member_map_.get(job))
        print("end:", job)
        return True
    else:
        return False

def main():
    remain_jobs = job_list
    for job in job_list:
        start_job(job)

    while len(remain_jobs) > 0:
        for job in remain_jobs:
            if check_end(job):
                remain_jobs.remove(job)
                break

        time.sleep(1)


if __name__ == '__main__':
    logger.job_start(Job.MEMCACHED, [0, 1], 2)
    main()
    logger.end()