import time
import subprocess
import psutil
import docker
from scheduler_logger import SchedulerLogger, Job

client = docker.from_env()

# Stop and remove each container
containers = client.containers.list(all=True)
for container in containers:
    container.stop()
    container.remove()

logger = SchedulerLogger()

job_list = [
    "blackscholes",
    "canneal",
    "dedup",
    "ferret",
    "freqmine",
    "radix",
    "vips"
]

thread_map = {
    "blackscholes": 2,
    "canneal": 2,
    "dedup": 1,
    "ferret": 2,
    "freqmine": 3,
    "radix": 2,
    "vips": 3
}

start_cpuset_map = {
    "blackscholes": "1",
    "canneal": "1",
    "dedup": "1",
    "ferret": "2-3",
    "freqmine": "2-3",
    "radix": "2-3",
    "vips": "2-3"
}

_str2list_map = {
    "0": [0],
    "1": [1],
    "0-1": [0, 1],
    "2-3": [2, 3],
    "1-3": [1, 2, 3]
}

def start_job(job):
    image = f"anakli/cca:{'splash2x' if job == 'radix' else 'parsec'}_{job}"
    command = f"./run -a run -S {'splash2x' if job == 'radix' else 'parsec'} -p {job} -i native -n {thread_map[job]}"
    client.containers.run(
        image=image,
        name=job,
        detach=True,
        cpuset_cpus=start_cpuset_map[job],
        command=command
    )
    logger.job_start(Job._value2member_map_.get(job), _str2list_map[start_cpuset_map[job]], thread_map[job])

def update_cores(job, new_cores: str):
    if job == "memcached":
        subprocess.run(['bash', '-c', f"pid=$(pidof memcached) && sudo taskset -a -cp {new_cores} $pid"])
    else:
        container = client.containers.get(job)
        container.update(cpuset_cpus=new_cores)
    logger.update_cores(Job._value2member_map_.get(job), _str2list_map[new_cores])

def pause_job(job):
    if job == "":
        print("fail pause: no job")
        return
    container = client.containers.get(job)
    if(container.attrs['State']['Status'] == "running"):
        container.pause()
        logger.job_pause(Job._value2member_map_.get(job))
    else:
        print("fail pause:", job)

def unpause_job(job):
    if job == "":
        print("fail unpause: no job")
        return
    container = client.containers.get(job)
    if(container.attrs['State']['Status'] == "paused"):
        container.unpause()
        logger.job_unpause(Job._value2member_map_.get(job))
    else:
        print("fail unpause:", job)

def check_end(job) -> bool:
    if job == "":
        return False
    container = client.containers.get(job)
    if(container.attrs['State']['Status'] == "exited"):
        logger.job_end(Job._value2member_map_.get(job))
        return True
    else:
        return False
        
def get_next_state(state) -> int:
    count = 3
    cpu_percent = psutil.cpu_percent(interval=0.3, percpu=True)
    if state == 0:
        cpu = cpu_percent[0]
        print("state:0 --------------", cpu)
        if cpu < 60:
            return 0
        else:
            return 1
    else:
        cpu = cpu_percent[0] + cpu_percent[1]
        print(f"state:{state} --------------", cpu)
        if cpu < 40:
            return (state + 1) % count
        else:
            return 1

core1_job_list = ["blackscholes", "canneal", "dedup"]
core23_job_list = ["ferret", "radix", "vips", "freqmine"]
core1_running_time_map = {"": 999, "dedup": 999, "blackscholes": 12, "canneal": 40}

def main():
    end_job_list = []
    core1_current_job = ""
    
    core23_paused_job_list = []
    core23_running_job = core23_job_list.pop(0)
    start_job(core23_running_job)

    ts = int(time.time())
    core1_running_time = 0
    state = 1
    next_state = 1
    while not (core1_current_job == "" and len(core1_job_list) == 0):
        if check_end(core23_running_job):
            end_job_list.append(core23_running_job)
            if len(core23_paused_job_list) > 0:
                core23_running_job = core23_paused_job_list.pop(0)
                unpause_job(core23_running_job)
            else:
                core23_running_job = core23_job_list.pop(0)
                start_job(core23_running_job)

        next_state = get_next_state(state)
        if state == 0 and next_state == 1:
            update_cores("memcached", "0-1")
            pause_job(core1_current_job)        
            core1_running_time += (int(time.time()) - ts)
            print("core1_running_time:", core1_running_time)
            time.sleep(1)
        elif state > 0 and next_state == 0:
            update_cores("memcached", "0")
            if not core1_current_job == "":
                unpause_job(core1_current_job)
                ts = int(time.time())
            elif len(core1_job_list) > 0:
                core1_current_job = core1_job_list.pop(0)
                start_job(core1_current_job)
                core1_running_time = 0
                ts = int(time.time())
        state = next_state

        if check_end(core1_current_job):
            end_job_list.append(core1_current_job)
            core1_current_job = ""
        elif core1_running_time > core1_running_time_map[core1_current_job]:
            update_cores(core1_current_job, "2-3")
            if core23_running_job == "vips" or core23_running_job == "freqmine":
                pause_job(core23_running_job)
                core23_paused_job_list.append(core23_running_job)
                core23_running_job = core1_current_job
                unpause_job(core23_running_job)
                core1_current_job = ""
            else:
                core23_paused_job_list.append(core1_current_job)
                core1_current_job = ""

        # time.sleep(0.5)

    print("---------stage 1 complete---------")
    print("core23_paused_job_list:", core23_paused_job_list)
    print("core23_running_job:", core23_running_job)
    print("end_job_list:", end_job_list)
    print("---------stage 1 complete---------")

    current_job = core23_running_job
    job_list = core23_job_list
    if state == 0:
        update_cores(current_job, "1-3")

    while True:
        if check_end(current_job):
            if len(core23_paused_job_list) > 0:
                current_job = core23_paused_job_list.pop(0)
                unpause_job(current_job)
            elif len(job_list) > 0:
                current_job = job_list.pop(0)
                start_job(current_job) 
            else:
                break

        next_state = get_next_state(state)
        if state == 0 and next_state == 1:
            update_cores("memcached", "0-1")
            update_cores(current_job, "2-3")
            time.sleep(1)
        elif state > 0 and next_state == 0:
            update_cores("memcached", "0")
            update_cores(current_job, "1-3")
        state = next_state

        # time.sleep(0.5)

    if state == 0:
        update_cores("memcached", "0-1")      

if __name__ == '__main__':
    logger.job_start(Job.MEMCACHED, [0, 1], 2)
    main()
    logger.end()